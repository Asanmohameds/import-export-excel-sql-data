﻿using ImportAndExportData.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace ImportAndExportData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportAndExportDatasController : ControllerBase
    {
        IConfiguration configure;

        public ImportAndExportDatasController(IConfiguration configuration)
        {
            configure = configuration;
        }

        [HttpPost("export")]
        public IActionResult ExportDataFromExcelToSqlDb([FromForm] Filedata filesdata)
        {
            

            var list = new List<Student>();
            var stream = new MemoryStream();
            filesdata.files.CopyTo(stream);

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(stream);
            var ExcelSheet = package.Workbook.Worksheets[0];

            int rowCount = ExcelSheet.Dimension.Rows;
            for (int i = 2; i < rowCount; i++)
            {
                list.Add(new Student
                {
                    StudentId = ExcelSheet.Cells[i, 1].Value.ToString().Trim(),
                    StudentName = ExcelSheet.Cells[i, 2].Value.ToString().Trim(),
                    DateOfBirth = ExcelSheet.Cells[i, 3].Value.ToString().Trim(),
                    Marks = ExcelSheet.Cells[i, 4].Value.ToString().Trim(),
                });
            }

            foreach (Student item in list)
            {
                string DOB = DateTime.Parse(item.DateOfBirth).ToString("dd-MM-yyyy");
                string query = "insert into STUDENT values('" + item.StudentId + "', '" + item.StudentName + "','" + DOB + "','" + item.Marks + "')";

                string serverPath = configure.GetConnectionString("Data");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection con = new SqlConnection(serverPath))
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        con.Close();
                        reader.Close();
                    }
                }

            }

            return Ok("Updated to DB table successfully");
        }


        [HttpGet("import")]
        public IActionResult ImportDataToExcelFromSqlDb()
        {
            try
            {
                

                string query = "select * from STUDENT";
                DataTable table = new DataTable();
                SqlDataReader reader;

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                string server = configure.GetConnectionString("Data");
                SqlConnection connection = new SqlConnection(server);

                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet db = new DataSet();
                adapter.Fill(db);

                var stream = new MemoryStream();

                ExcelPackage package = new ExcelPackage(stream);
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromDataTable(db.Tables[0], true);
                package.Save();
                stream.Position = 0;
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "StudentDatas.xlsx");

            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
        }



    }
}
